.PHONY: default
default: build

.PHONY: build
build:
	dune build @install

.PHONY: clean
clean:
	dune clean

.PHONY: js
js:
	dune build exe/FlowConfigExe.bc.js
