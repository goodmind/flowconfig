module Js = Js_of_ocaml.Js

let parse contents = 
  let contents = Js.to_string contents in
  let config = FlowConfig.get_empty_config () in
  let lines = contents
    |> String.split_on_char '\n'
    |> List.mapi (fun i line -> (i+1, String.trim line))
    |> List.filter FlowConfig.is_not_comment
  in
  match FlowConfig.parse config lines with
    | Error _ -> failwith "parse error"
    | Ok (config, warnings) -> object%js (self)
      val config = FlowConfig.to_js config
      val warnings = Js.array (Array.of_list warnings)
    end

let () = Js.export
  "parse" (Js.wrap_callback parse)
