(**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *)

type esproposal_feature_mode =
  | ESPROPOSAL_ENABLE
  | ESPROPOSAL_IGNORE
  | ESPROPOSAL_WARN
  [@@deriving show { with_path = false }]

type file_watcher =
  | NoFileWatcher
  | DFind
  | Watchman
  [@@deriving show { with_path = false }]

type module_system =
  | Node
  | Haste

type lazy_mode =
  | LAZY_MODE_FILESYSTEM
  | LAZY_MODE_IDE
  | LAZY_MODE_WATCHMAN
  | NON_LAZY_MODE

type saved_state_fetcher =
  | Dummy_fetcher
  | Local_fetcher
  | Fb_fetcher

type arch =
  | Classic
  | TypesFirst

type trust_mode =
  | NoTrust
  | CheckTrust
  | SilentTrust
  [@@deriving show { with_path = false }]

let lazy_mode_to_string lazy_mode =
  match lazy_mode with
  | LAZY_MODE_FILESYSTEM -> "fs"
  | LAZY_MODE_IDE -> "ide"
  | LAZY_MODE_WATCHMAN -> "watchman"
  | NON_LAZY_MODE -> "none"
