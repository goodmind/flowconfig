//@flow

const flowconfig = require('./_build/default/exe/FlowConfigExe.bc.js')

const config = String.raw`
[ignore]
<PROJECT_ROOT>/flow/google-protobuf
<PROJECT_ROOT>/.history/.*
<PROJECT_ROOT>/node_modules/popper.js/.*

[lints]
all=warn
dynamic-export=off
untyped-type-import=error

[options]
experimental.const_params=true
esproposal.optional_chaining=enable
esproposal.nullish_coalescing=enable
esproposal.export_star_as=enable
include_warnings=false
suppress_type=$off
suppress_type=$todo
module.ignore_non_literal_requires=true
module.use_strict=true
module.file_ext=.js
module.file_ext=.json
module.file_ext=.jsx
module.file_ext=.css

[strict]
nonstrict-import
unclear-type
unsafe-getters-setters
untyped-import
untyped-type-import
`

const data = flowconfig.parse(config.trim())

console.log(data.warnings)
console.log(data.config.options.suppressComments)